# Violence detection in video using combined training datasets and a long short-term memory convolutional model
Public authorities have made use of video cameras as part of surveillance systems with one of their objectives being the rapid detection of violent actions, a task that is usually performed by human visual inspection, which is labor intensive. For this reason different Deep Learning models have been implemented to remove the human eye from this task and have yielded positive results. When it comes to violence detection in video, one of the problems faced is the variety of violent scenarios that can exist, which has led to different models being trained on datasets that lead them to detect violence in only one type of videos. In this work we present a series of models, trained on individual datasets as well as the union of them, capable of detecting violence in different environments. The models themselves are hybrid neural networks that use transfer learning to adapt a feature extraction technique based on the DenseNet121 convolutional neural network architecture; which has been successfully implemented in the detection of of violence in video files. Additionally, the model makes use of a bidirectional LSTM network to aid with the classification. The results obtained show that it is possible to make use of the model to detect violence in different scenarios when it is trained using a robust dataset depicting different environments.

# Dependencies
- h5py==3.9.0
- kaggle==1.5.16
- keras==2.9.0
- matplotlib==3.7.2
- numpy==1.25.2
- opencv-python-headless==4.8.0.76
- Pillow==10.0.0
- scikit-learn==1.3.0
- scipy==1.11.2
- tensorflow==2.9.0
- tensorflow-addons==0.21.0
- tensorflow-gpu==2.9.0
- torch==2.0.1
- torchvision==0.15.2
